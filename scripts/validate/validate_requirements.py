#!/usr/bin/python3

import os
import sys
import json
import argparse
import jsonschema
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from json_templates import ksat_list_template
import helpers

work_roles = []

avoid_list_default = None
avoid_list_desc = "A list of files to ignore when building the working file list. " \
                  "The default is '{0}'.".format(avoid_list_default)
description = "This validates one or more requirement files & the TRN_ID/EVL_ID within each requirement file."

default_wr_spec_keys = ['work-roles', 'specializations']
default_file_list = ['Roadmap.json']
validation_files_desc = "Specify a list of JSON files that have the 'trn_rel_link_urls' and 'evl_rel_link_urls'"

keyword_default = ".json"
keyword_desc = "A keyword to specify the returned file paths for processing. " \
               "The default is '{0}'.".format(keyword_default)
log_file_default = "ValidateTrnEvlIdLog.json"
log_file_desc = "The name of the log file; the default is '{0}'".format(log_file_default)
out_dir_default = "."
out_dir_desc = "The directory to store the output 'log_file'. Note, the directory is created if it doesn't exist. " \
               "By default files are written to {0}.".format(log_file_default, out_dir_default)
start_dir_default = "./requirements"
start_dir_desc = "This is the starting directory to the requirements. The default is '{0}'.".format(start_dir_default)

parser = argparse.ArgumentParser(description=description)
parser.add_argument("-a", "--avoid_list", type=list, default=avoid_list_default, help=avoid_list_desc)
parser.add_argument("-k", "--keyword", type=str, default=keyword_default, help=keyword_desc)
parser.add_argument("-l", "--log_file", type=str, default=log_file_default, help=log_file_desc)
parser.add_argument("-o", "--out_dir", type=str, default=out_dir_default, help=out_dir_desc)
parser.add_argument("-s", "--start_dir", type=str, default=start_dir_default, help=start_dir_desc)
parser.add_argument("-f", "--validation_files", type=list, default=default_file_list, help=validation_files_desc)

log = {}
# This represents the general keys/values required in a requirement (K, S, A or T) JSON file
req_schema = ksat_list_template(work_roles)

def validate_req_file(valid_work_roles: list, req_data: dict, req_file: str, role_keys: list) -> dict:
    '''
    Purpose: This function validates a single requirement JSON file against its expected schema as well as other
    desired value checks.
    :param valid_work_roles: A list of valid WORK-ROLES/SPECIALIZATIONS for comparison
    :param req_data: A dictionary representing a specific requirement JSON file
    :param req_file: The name of the requirement JSON file
    :param role_keys: A list of keys that needs work-role/specialization ID validation
    :return: Success = {}; otherwise, a list of errors
    '''
    req_log = {}
    req_id = ""

    try:
        # Allow collecting all validation errors by using separate try blocks
        jsonschema.validate(instance=req_data, schema=req_schema)  # Check requirement file generally
    except jsonschema.ValidationError as err:
        req_log["Validation Error(s) in {0}".format(os.path.basename(req_file))] = [
            "Failed validating '{0}': {1}".format(err.validator, err.message),
            "On instance '{0}'".format(list(err.absolute_path)),
            "In schema '{0}'".format(list(err.schema_path))
        ]
    try:
        # This checks the value of the TRN_EVL_IDs to be sure they're valid
        for req in req_data:        
            errs = {}  # Lump all errors for this ID into one log item.
            for role_key in role_keys:
                # Validate the work-role/specialization IDs within each requirement
                # errs.update(validate_work_roles(valid_work_roles, req[role_key], role_key, req['_id']))
                if len(errs) > 0:
                    err_msg = "Error while parsing '{req_file}' ID '{KSAT_ID}'".format(
                        req_file=os.path.basename(req_file),
                        KSAT_ID=req['_id']
                    )
                    req_log[err_msg] = errs
    except (ValueError, KeyError, TypeError) as err:
        err_msg = "Error while parsing '{req_file}' ID '{KSAT_ID}'".format(
            req_file=os.path.basename(req_file),
            KSAT_ID=req['_id']
        )
        req_log[err_msg] = ["{0}".format(err)]  # Converts object to string
    return req_log


def main() -> int:
    '''
    Purpose: This is responsible for collecting all requested input as well as valid data for comparison in order to
    validate each requirement file within the MTTL. Error statements are kept in a list called log.
    :return: Success = 0; errors = 1
    '''
    global args, log, work_roles

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    if args.avoid_list is None:
        args.avoid_list = []

    for path in args.validation_files:
        try:
            with open(path, "r") as wrspec_file:
                work_roles += list(json.load(wrspec_file).keys())
        except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError) as err:
            err_msg = "Unable to load JSON data in '{0}'".format(path)
            print("{0}: {1}.".format(err_msg, err))
            log[err_msg] = ["{0}".format(err)]  # Converts object to string
            return 1

    req_files = helpers.get_all_json(args.start_dir, args.keyword, args.avoid_list)

    for req_file in req_files:
        try:
            with open(req_file, "r") as req_fp:
                req_data = json.load(req_fp)
        except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError) as err:
            err_msg = "Unable to load JSON data in {0}".format(req_file)
            print("{0}: {1}.".format(err_msg, err))
            log[err_msg] = ["{0}".format(err)]  # Converts object to string
        else:
            # Validate this requirement JSON only if the json.load was successful
            this_status = validate_req_file(work_roles, req_data, req_file, default_wr_spec_keys)
            if len(this_status) > 0:
                print(this_status)
            log.update(this_status)

    if len(log) > 0:
        return 1
    else:
        return 0


if __name__ == "__main__":
    args = parser.parse_args()
    status = main()
    if status == 0:
        msg = "All requirements are valid."
        print(msg)
        log[status] = msg
    else:
        print("Check {0} for validation errors.".format(os.path.join(args.out_dir, args.log_file)))

    with open(os.path.join(args.out_dir, args.log_file), "w") as log_fp:
        json.dump(log, log_fp, indent=4)

    sys.exit(status)
