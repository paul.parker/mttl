#!/usr/bin/python3  

import os
import sys
import jsonschema
import json
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json
from json_templates import trn_rel_link_item_template, evl_rel_link_item_template
from mttl_cmds.insert import validate_insert_data

evl_urls = {
    "basic-dev": "https://gitlab.com/90cos/public/evaluations/preparatory/basic-dev",
    "basic-po": "https://gitlab.com/90cos/public/evaluations/preparatory/basic-po",
    "senior-dev-linux": "https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-linux",
    "senior-dev-windows": "https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-windows"
}

trn_urls = {
    "python": "https://gitlab.com/90cos/cyt/training/modules/python",
    "c-programming": "https://gitlab.com/90cos/cyt/training/modules/c-programming",
    "reverse-engineering": "https://gitlab.com/90cos/cyt/training/modules/reverse-engineering",
    "algorithms": "https://gitlab.com/90cos/cyt/training/modules/algorithms",
    "powershell": "https://gitlab.com/90cos/cyt/training/modules/powershell",
    "debugging": "https://gitlab.com/90cos/cyt/training/modules/debugging",
    "pseudocode": "https://gitlab.com/90cos/cyt/training/modules/pseudocode",
    "cpp-programming": "https://gitlab.com/90cos/cyt/training/modules/cpp-programming",
    "introduction-to-git": "https://gitlab.com/90cos/cyt/training/modules/introduction-to-git",
    "assembly": "https://gitlab.com/90cos/cyt/training/modules/assembly",
    "network-programming": "https://gitlab.com/90cos/cyt/training/modules/network-programming",
    "scrum.org-professional-scrum-product-owner-i": "https://gitlab.com/90cos/cyt/training/courses/external/scrum.org-professional-scrum-product-owner-i",
    "actp-linux": "https://gitlab.com/90cos/cyt/training/courses/external/actp-linux",
    "see-basics": "https://gitlab.com/90cos/cyt/training/material/see-basics"
}


def convert_ksat_list(mappings: list, url: str):
    new_list = []
    for mapping in mappings:
        tmp_dict = {}
        tmp_dict['ksat_id'] = mapping[0]
        try:
            tmp_dict['item_proficiency'] = mapping[1]
        except:
            tmp_dict['item_proficiency'] = ""
        tmp_dict['url'] = url
        new_list.append(tmp_dict)
    return new_list

def main():
    rels = []
    trn = 'training-repos'
    evl = 'eval-repos'

    for rel_link in get_all_json(trn, '.rel-link.json'):
        with open(rel_link) as rel_file:
            rel_json = json.load(rel_file)

        try: 
            rel_json['module_id'] = rel_json['uid']
            del rel_json['uid']
        except KeyError:
            pass
        try: 
            rel_json['network'] = rel_json['NETWORK']
            del rel_json['NETWORK']
        except KeyError:
            pass
        try: 
            rel_json['course'] = rel_json['TRN_EVL_ID']
            del rel_json['TRN_EVL_ID']
        except KeyError:
            pass
        try: 
            del rel_json['lesson']
        except KeyError:
            pass
        rel_json['module'] = rel_link.split('/')[-1].split('.')[0]
        if 'topic' not in rel_json:
            rel_json['topic'] = ""
        if 'subject' not in rel_json:
            rel_json['subject'] = ""
        
        try: 
            rel_json['work-roles'] = rel_json['workroles']
            del rel_json['workroles']
        except KeyError:
            rel_json['work-roles'] = []

        # convert KSATs to list of dicts
        try:
            rel_path_list = rel_link.split('/')
            base_url = trn_urls[rel_path_list[1]]
            rel_path = '/'.join(rel_path_list[2:])
            url = f'{base_url}/-/tree/master/{rel_path}'
            rel_json['KSATs'] = convert_ksat_list(rel_json['KSATs'], url)
        except KeyError:
            pass

        # add the map_for key:val
        rel_json['map_for'] = 'training'
        
        try:
            validate_insert_data(rel_json, trn_rel_link_item_template)
        except jsonschema.ValidationError as err:
            print(err)
            exit(1)

        # add rel link to the list
        rels.append(rel_json)


    for rel_link in get_all_json(evl, '.rel-link.json'):
        with open(rel_link) as rel_file:
            rel_json = json.load(rel_file)

        if 'test_id' not in rel_json:
            rel_json['test_id'] = ""
        if 'test_type' not in rel_json:
            rel_json['test_type'] = ""

        # convert old keys to new template             
        try: 
            rel_json['question_id'] = rel_json['uid']
            del rel_json['uid']
        except KeyError:
            pass
        try: 
            rel_json['network'] = rel_json['NETWORK']
            del rel_json['NETWORK']
        except KeyError:
            pass
        rel_json['question_name'] = rel_link.split('/')[-1].split('.')[0]
        try: 
            rel_json['language'] = rel_json['version']
            del rel_json['version']
            if rel_json['language'] == None:
                rel_json['language'] = ""    
        except KeyError:
            rel_json['language'] = ""
        try: 
            rel_json['question_proficiency'] = rel_json['proficiency']
            del rel_json['proficiency']
        except KeyError:
            pass
        try: 
            rel_json['estimated_time_to_complete'] = rel_json['etc']
            del rel_json['etc']
        except KeyError:
            pass
        try: 
            rel_json['created_on'] = rel_json['creation_date']
            del rel_json['creation_date']
        except KeyError:
            pass
        try: 
            rel_json['updated_on'] = rel_json['revision_date']
            del rel_json['revision_date']
        except KeyError:
            pass
        try: 
            rel_json['work-roles'] = rel_json['workroles']
            del rel_json['workroles']
            if 'knowledge' in rel_json:
                rel_json['test_id'] = f'{rel_json["work-roles"][0]}'
                rel_json['test_type'] = 'knowledge'
            elif 'performance' in rel_json:
                rel_json['test_id'] = f'{rel_json["work-roles"][0]}'
                rel_json['test_type'] = 'performance'
        except KeyError:
            pass

        # convert KSATs to list of dicts
        try:
            rel_path_list = rel_link.split('/')
            base_url = evl_urls[rel_path_list[1]]
            rel_path = '/'.join(rel_path_list[2:])
            url = f'{base_url}/-/tree/master/{rel_path}'
            rel_json['KSATs'] = convert_ksat_list(rel_json['KSATs'], url)
        except KeyError:
            pass

        # add the map_for key:val
        rel_json['map_for'] = 'eval'
        # remove question content from the rel links
        try:
            del rel_json["knowledge"]
        except KeyError:
            pass
        try:
            del rel_json["performance"]
        except KeyError:
            pass

        # delete unneeded fields
        try:
            del rel_json["checksum"]
        except KeyError:
            pass
        try:
            del rel_json["TRN_EVL_ID"]
        except KeyError:
            pass

        try:
            validate_insert_data(rel_json, evl_rel_link_item_template)
        except jsonschema.ValidationError as err:
            print(err)
            exit(1)

        # add rel link to the list
        rels.append(rel_json)

    with open("all-rel-link.json", "w") as rel_write:
        json.dump(rels, rel_write, sort_keys=False, indent=4)

if __name__ == "__main__":
    main()