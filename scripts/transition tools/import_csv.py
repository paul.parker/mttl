#!/usr/bin/python3

import os
import csv
import re
import pymongo

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

req_src = 'ACC CCD CDD TTL'

def match_regex(pat:str, item:str):
    return re.match(pat, item)

def modify_ksat(reqs:object, ksat_id:str, parents:list):
    new_parents = []
    for parent in parents:
        if match_regex('^[KSAT][0-9]{4}$', parent):
            new_parents.append(parent)
    return reqs.update_one(
        {'_id': ksat_id},
        {
            '$addToSet': {
                'parent': { '$each': new_parents },
                'requirement_src': req_src
            } 
        }
    )

def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    for ksat in reqs.find({ 'requirement_src': ["ACC CCD CDD TTL"] }):
        reqs.update_one(
            {'_id': ksat['_id']},
            {'$set': {'requirement_owner': "ACC A2/3/6K"}}
        )

if __name__ == "__main__":
    main()