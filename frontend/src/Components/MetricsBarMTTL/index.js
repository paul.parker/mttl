import React, {Component} from 'react'
import './MetricsBarMTTL.scss'
import ProgressBar from 'react-bootstrap/ProgressBar'

class MetricsBarMTTL extends Component {
    createProgressBar = (data) => {
        let percent_data = [
            {'percent': data['percent-total'], 'type': 'training-eval'}, 
            {'percent': data['percent-training'], 'type': 'training'}, 
            {'percent': data['percent-eval'], 'type': 'eval'}
        ];
        // sort the list <
        percent_data.sort((a, b)=> {return a['percent']-b['percent']});

        let sum = 0;
        let totalp = 0;
        let progressBars = []
        percent_data.forEach((val, i, arr) => {
            let percent = Math.round(val['percent']);
            const level = Math.round(percent - sum);
            totalp += level;
            progressBars.push(<ProgressBar variant={val['type']} now={level} label={`${level}%`} key={i+1} />);
            if (i === 0) {
                sum = Math.round(val['percent'])
            }
        })
        // not covered by both
        let not_cov = 100 - totalp;
        progressBars.push(<ProgressBar variant={'not_cov'} now={not_cov} label={`${not_cov}%`} key={4} />)

        return (
            <ProgressBar>
                { progressBars }
            </ProgressBar>
        );
    } 

    render() {
        return (
            <div id="metrics_bar_mttl">
                {this.createProgressBar(this.props.data)}
            </div>
            
        );
    }
}

export default MetricsBarMTTL;