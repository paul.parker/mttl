// Example of importing a file and assigning it to the PDFs array.
// import * as Basic_PO from '../data/JQS/Basic-PO-OJT.pdf';
// import * as Senior_PO from '../data/JQS/Senior-PO-OJT.pdf';
// import * as Basic_Dev from '../data/JQS/Basic-Dev-OJT.pdf';
// import * as Senior_Dev_Linux from '../data/JQS/Senior-Dev-Linux-OJT.pdf';
// import * as Senior_Dev_Windows from '../data/JQS/Senior-Dev-Windows-OJT.pdf';
// import * as Test_Automation_Engineer from '../data/JQS/Test-Automation-Engineer-OJT.pdf';

let PDFs = {};
// PDFs['Basic-PO'] = Basic_PO;
// PDFs['Senior-PO'] = Senior_PO;
// PDFs['Basic-Dev'] = Basic_Dev;
// PDFs['Senior-Dev-Linux'] = Senior_Dev_Linux;
// PDFs['Senior-Dev-Windows'] = Senior_Dev_Windows;
// PDFs['Test-Automation-Engineer'] = Test_Automation_Engineer;

// vvvvvvv WORKING but fails the pipleine vvvvvvvv
// This commented for now because it works locally but the NPM build command breaks
// PDFs['Basic-PO'] = require('../data/JQS/Basic-PO-OJT.pdf');
// PDFs['Senior-PO'] = require('../data/JQS/Senior-PO-OJT.pdf');

// PDFs['Basic-Dev'] = require('../data/JQS/Basic-Dev-OJT.pdf');

// PDFs['Senior-Dev-Linux'] = require('../data/JQS/Senior-Dev-Linux-OJT.pdf');
// PDFs['Senior-Dev-Windows'] = require('../data/JQS/Senior-Dev-Windows-OJT.pdf');

// PDFs['Test-Automation-Engineer'] = require('../data/JQS/Test-Automation-Engineer-OJT.pdf');
// ^^^^^^^^^WORKING^^^^^^

// Specializations, if ever needed
// Basic-SEE
// Mobile
// Embedded
// Networking
// Vulnerability-Research

export default PDFs