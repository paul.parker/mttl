Please make sure to search the MTTL prior to submitting a new KSAT.
For best practices concerning KSAT creation, see [here](https://gitlab.com/90cos/mttl/-/blob/master/CONTRIBUTING.md#addkast)

### Why does the requirement(s) need to be added?:
For example: 
 - Further defining the job
 - Adding a new Item to the Job
 - I'm a program owner and need people trained on something
 - I want to further refine a parent KSAT

### KSAT Description(s):
This is a single sentence description of the requirement.  It's helpful to think of having "After training the student should be able to... **put your language here**"

Examples
"List the flight commanders in the 90th." OR
"Write pseudocode to solve a programming problem."

### Related KSATs
Does this KSAT relate to any others?

Parent KSATs: Please list more generalized KSAT that necessitate this KSAT i.e. of which this KSAT is a component.

Child KSAT: Please list KSATs that are constituent elements of this KSAT.

### Training Reference (optional)
Do you have one or more references that explain or are necessary for the Knowledge, Skills, Abilities, or Tasks suggested by the new KSAT?
For example, the 90COS Org Chart (Reference listing flight commanders) might be required for a 90COS specific KSAT.

### Workrole alignment
Which work roles does this requirement apply to?:



### Related Requirements/Documentation
Is this requirement driven by an external document such as an AFI, Law, or other similar requirement?  
If so, please list relevant documents, e.g. 17-202v2 states that all SEE's must be trained on objectivity.


/label ~"mttl::ksat" ~customer ~"office::CYT" ~"backlog::idea"
/milestone %"CYT Backlog"
