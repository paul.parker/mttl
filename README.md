#  MTTL 
## The Master Training Task List 

This application provides access to the ***Master Training Task List*** (**MTTL**), a reference for work role requirements across the USAF, especially the 90th Cyber Operations Squadron, as defined by US Cyber Command and other relevant stakeholders.  
The MTTL provides a listing of the *Knowledge*, *Skills*, *Abilities*, and *Tasks* (***KSATs***) that should be mastered for various workroles and specializations across the 90th and other USAF Squadrons.  

The **MTTL** provides an overview of work role requirements and mappings to training and evaluation resources related to those work role requirements.  
The **MTTL** is intended as a resource for those designing and implementing training and evaluation i.e. at the 39th and the 90th.  
The **MTTL** can also support flight commanders in understanding crew readiness,  capabilities, and training expectations given work role requirements.   
 
--- 

The *soon to be deprecated* production build for the MTTL is located [here](https://90cos.gitlab.io/mttl/#/)  
A newer, soon to be released, MTTL is [here](https://90cos-cyt-training-systems-angular-mttl.90cos.com/mttl) and it's gitlab page is [here](https://gitlab.com/90cos/cyt/training-systems/angular-mttl)      
For documentation, please go [here](https://90cos-cyt-training-systems-angular-mttl.90cos.com/documentation)   
[*See explanation below*.]  

***************************************************************************************  
***************************************************************************************  
# ** **Notice:**   
##  ** **The MTTL is currently under revision**.  
** ***The most thorough documents** are [here](https://90cos-cyt-training-systems-angular-mttl.90cos.com/documentation).***     
** The database specific documentation i.e. MongoDb, JSON files, and how to edit them is the same for both versions of the MTTL.     
** ***To contribute KSATs or to query the MTTL, we recommend visiting [the new Angular MTTL](https://90cos-cyt-training-systems-angular-mttl.90cos.com/mttl )***    

***************************************************************************************  
***************************************************************************************  



--- 
## For developers 
### To build locally: 
1. Follow [these instructions](https://gitlab.com/90cos/mttl/-/wikis/How-to's-(Developer)/1.-Development-Environment-Setup) to get Docker running with VSCode
1. Clone this repo 
1. Launch VSCode within the newly cloned directory and select "run in container" (this step may take a minute)
1. Press ctrl+` or select View->Terminal
In the terminal, you will want to:
1. [Run the pipeline script](https://gitlab.com/90cos/mttl/-/wikis/How-to's-(Developer)/2.-Run-the-Pipeline-in-the-Dev-Environment) i.e. `./scripts/run_pipeline_scripts.sh`
1. [Run the frontend](https://gitlab.com/90cos/mttl/-/wikis/How-to's-(Developer)/3.-Run-the-Frontend-in-the-Dev-Environment) i.e. 
```bash 
cd frontend
yarn install
yarn start
```
If everything was done correctly, a local build of the mttl should be available at http://localhost:3000/mttl  


## ***Next steps, most likely you will want to:***  
 1. add to the JSON files found in `rel-links/` , `requirements/`, or `work-roles/`
 1. most revision is done per hand - pay attention to matching curl brackets. 
 1. addition and deletion of KSATs and rel-links are accomplished through the scripts described [here](https://gitlab.com/90cos/mttl/-/wikis/How-to's-(Developer)/7.-mttl.py-Usage)  

 File characteristics, expected data types, and file templates can be found [here](https://90cos-cyt-training-systems-angular-mttl.90cos.com/documentation/files/index.html)  

--- 

## To contribute to the MTTL:
1.  Review the schema to ensure you have the necessary fields for your contribution, please see the following link: 
https://gitlab.com/90cos/mttl/-/wikis/home
2.  a) Click the "Issues" button on the left side bar.

	b) Create a new issue by clicking the "New Issue" button.
![Issue](.wiki/mdbook/src/upload/issue.png)

3.  Select the template that best matches the changes that you are proposing from the "Description" dropdown.

    a) To propose a new workrole, select the "New_Work_Role" template.
    
    b) To propose general changes, select the "General_Contribution" template.
4.  a) Provide a useful title that we can track.

	b) Replace the text wrapped in parentheses with the required information.
	
	c) Provide a Label in the "Labels" dropdown.

Note: ***This is intended to gather the contributing information and not how to implement the contributors changes into the MTTL.***



## Pipeline Overview

| Training/Test Bank | Lesson | Pipeline Status | Study Repo Status |
| ---           | ------ | --------------- | ----------------- |
| Training | Python | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/python/badges/master/pipeline.svg)](https://gitlab.com/90cos/cyt/training/modules/python/-/commits/master) | N/A |
| Training | C-Programming | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/C-Programming/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/C-Programming/-/commits/master) | N/A |
| Training | Network-Programming | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/network-programming/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/network-programming/-/commits/master) | N/A |
| Training | Introduction to Git | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/introduction-to-git/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/introduction-to-git/-/commits/master) | N/A |
| Training | Cpp-Programming | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/cpp-programming/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/cpp-programming/-/commits/master) | N/A |
| Training | Pseudocode | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/pseudocode/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/pseudocode/-/commits/master) | N/A |
| Training | Debugging | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/debugging/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/debugging/-/commits/master) | N/A |
| Training | Powershell | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/powershell/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/powershell/-/commits/master) | N/A |
| Training | Algorithms | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/algorithms/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/algorithms/-/commits/master) | N/A |
| Training | Reverse-Engineering | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/reverse-engineering/badges/master/pipeline.svg)](https://gitlab.com/90coscyt//training/modules/reverse-engineering/-/commits/master) | N/A |
| Training | Assembly | [![pipeline status](https://gitlab.com/90cos/cyt/training/modules/assembly/badges/master/pipeline.svg)](https://gitlab.com/90cos/cyt/training/modules/assembly/-/commits/master) | N/A |
| Test Bank | CCD | [![pipeline status](https://gitlab.com/90cos/Private-test-bank/basic-dev/badges/master/pipeline.svg)](https://gitlab.com/90cos/Private-test-bank/basic-dev/-/commits/master) | N/A |
| Test Bank | PO | [![pipeline status](https://gitlab.com/90cos/Private-test-bank/basic-po/badges/master/pipeline.svg)](https://gitlab.com/90cos/Private-test-bank/basic-po/-/commits/master) | N/A |
| Test Bank | SCCD-L | [![pipeline status](https://gitlab.com/90cos/Private-test-bank/senior-dev-linux/badges/master/pipeline.svg)](https://gitlab.com/90cos/Private-test-bank/senior-dev-linux/-/commits/master) | N/A |
| Test Bank | SCCD-W | [![pipeline status](https://gitlab.com/90cos/Private-test-bank/senior-dev-windows/badges/master/pipeline.svg)](https://gitlab.com/90cos/Private-test-bank/senior-dev-windows/-/commits/master) | N/A | 

### Misc Pipelines

| Repo | Pipeline Status |
| ---           | ------ |
| Combined Template | [![pipeline status](https://gitlab.com/90cos/cyt/training-systems/templates/combined-template/badges/master/pipeline.svg)](https://gitlab.com/90cos/cyt/training-systems/templates/combined-template/-/commits/master) |
| MDBook Template | [![pipeline status](https://gitlab.com/90cos/cyt/training-systems/templates/combined-template/badges/master/pipeline.svg)](https://gitlab.com/90cos/cyt/training-systems/templates/combined-template/-/commits/master) |
| Slides Template | [![pipeline status](https://gitlab.com/90cos/cyt/training-systems/templates/slides-template/badges/master/pipeline.svg)](https://gitlab.com/90cos/cyt/training-systems/templates/slides-template/-/commits/master) |


